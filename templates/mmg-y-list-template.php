<?php
/**
 * @var \MmgAdsPlugin\Common\Partner\Common\FeedZoneRequest $feedZoneRequest
 */

?>

<div class="<?=$feedZoneRequest->getInboundParams()->getFeedZoneParams()->getTheme()?>">
    <div class="mmg-y-list-link">
        Sponsored Results for <?php echo $feedZoneRequest->getPartnerRequestFilters()->getKeyword() ?>
    </div>

    <?php
    /** @var \MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooAdResult $ad */
    foreach ($feedZoneRequest->getResultSet()->getAds() as $ad):
        ?>
        <div class="mmg-y-list-block">
            <div class="mmg-y-list-container">
                <div class="mmg-y-list-main">
                    <a class="mmg-y-list-title" href="<?php echo $ad->getAdUrl() ?>">
                        <?php echo $ad->getTitle(); ?>
                    </a>
                    <div class="mmg-y-list-description">
                        <?php echo $ad->getDescription(); ?>
                        <div class="mmg-y-list-description__display-url">
                            <a href="<?php echo $ad->getAdUrl() ?>">
                                <?php echo $ad->getDisplayUrl() ?>
                            </a>
                        </div>
                        <?php
                        /** @var  $extension \MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooExtensions */
                        if ($extension = $ad->getExtensions()):
                            ?>
                            <?php
                            if ($extension->getYahooSmartAnnotation()):
                                foreach ($extension->getYahooSmartAnnotation()->getPhrases() as $annotation):
                                    ?>
                                    <div class="mmg-y-list-description__display-url">
                                        <a rel="nofollow" href="<?php echo $ad->getTrackUrl() ?>" target="_blank">
                                            <?php echo $annotation ?>
                                        </a>
                                    </div>
                                <?php
                                endforeach;
                            endif;
                            ?>

                            <?php
                            if ($extension->getYahooCallout()):
                                foreach ($extension->getYahooCallout()->getPhrases() as $callout):
                                    ?>
                                    <div class="mmg-y-list-description__display-url">
                                        <a rel="nofollow" href="<?php echo $ad->getTrackUrl() ?>" target="_blank">
                                            <?php echo $callout ?>
                                        </a>
                                    </div>
                                <?php
                                endforeach;
                            endif;
                            ?>

                            <?php
                            if ($extension->getYahooTopAd()):
                                ?>
                                <div class="mmg-y-list-description__display-url">
                                    <a rel="nofollow" href="<?php echo $ad->getTrackUrl() ?>" target="_blank">
                                        <?php echo $extension->getYahooTopAd()->getText() ?>
                                    </a>
                                </div>
                            <?php
                            endif;
                            ?>

                            <?php
                            if ($extension->getYahoo4thAnnotation()):
                                ?>
                                <div class="mmg-y-list-description__display-url">
                                    <a rel="nofollow"
                                       href="<?php echo $extension->getYahoo4thAnnotation()->getTrackUrl() ?>"
                                       target="_blank">
                                        <?php echo $extension->getYahoo4thAnnotation()->getText() ?>
                                    </a>
                                </div>
                            <?php
                            endif;
                            ?>

                        <?php endif; ?>

                    </div>
                </div>
                <div class="mmg-y-list-links">
                    <?php
                    /** @var  $extension \MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooExtensions */
                    if ($extension = $ad->getExtensions()):
                        ?>

                        <?php if (count($extension->getYahooSitelinks()) > 0): ?>
                        <?php
                        /**
                         * @var $siteLink \MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooSiteLink;
                         */
                        foreach (array_slice($extension->getYahooSitelinks(), 0, 2) as $siteLink):
                            ?>
                            <a rel="nofollow" href="<?php echo $siteLink->getTrackUrl() ?>" target="_blank"
                               class="mmg__link-title">
                                <?php echo $siteLink->getText() ?>
                            </a>
                            <?php if (count($siteLink->getDescrLines()) > 0): ?>
                            <?php foreach ($siteLink->getDescrLines() as $descrLine): ?>
                                <a rel="nofollow" class="mmg__link-description"
                                   href="<?php echo $siteLink->getTrackUrl() ?>" target="_blank">
                                    <?php echo $descrLine ?>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif ?>
                    <?php endif; ?>

                </div>

                <div class="mmg-y-list-button-wrap">
                    <?php if ($extension = $ad->getExtensions()): ?>
                        <?php if ($action = $extension->getYahooAction()): ?>
                            <a class="mmg-y-list-button" href="<?php echo $action->getActionTrackUrl() ?>">
                                <span class="mmg-y-list-button__text">
                                    <?php echo $action->getActionText() ?>
                                </span>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>