<?php

namespace MmgAdsPlugin\Common\Service;

use MmgAdsPlugin\Common\InboundParams\InboundParams;
use MmgAdsPlugin\Common\Partner\Common\FeedZoneRequest;
use MmgAdsPlugin\Common\Partner\Common\PartnerApiInterface;
use MmgAdsPlugin\Common\Partner\Common\PartnerResultsFormatterInterface;
use MmgAdsPlugin\Common\Partner\Common\RequestFilterFactoryInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class PartnerAdsService
{
    private RequestFilterFactoryInterface $requestFilterFactory;
    private PartnerApiInterface $partnerApi;
    private PartnerResultsFormatterInterface $resultsFormatter;

    public function __construct(
        RequestFilterFactoryInterface $requestFilterFactory,
        PartnerApiInterface $partnerApi,
        PartnerResultsFormatterInterface $resultsFormatter
    )
    {
        $this->requestFilterFactory = $requestFilterFactory;
        $this->partnerApi = $partnerApi;
        $this->resultsFormatter = $resultsFormatter;
    }

    public function createFeedZoneRequest(
        InboundParams $inboundParams
    ): FeedZoneRequest
    {
        $requestFilters = $this->getRequestFilterFactory()->createRequestFilters($inboundParams);
        $feedZoneRequest = new FeedZoneRequest();
        $feedZoneRequest->setInboundParams($inboundParams);
        $feedZoneRequest->setPartnerRequestFilters($requestFilters);

        return $feedZoneRequest;
    }

    /**
     * @param FeedZoneRequest[] $feedZoneRequests
     * @return void
     */
    public function createResults(array $feedZoneRequests = [])
    {
        $responses = [];
        foreach($feedZoneRequests as $key => $feedZoneRequest) {
            if(is_null($feedZoneRequest->getPartnerRequestFilters())) {
                continue;
            }

            $responses[$key] = $this->getPartnerApi()->createResponse(
                $feedZoneRequest->getPartnerRequestFilters()
            );
        }

        /** @var ResponseInterface $response */
        foreach($responses as $key => $response) {
            try {
                $partnerApiResults = $this->getPartnerApi()->createResults($response);
                if($results = $this->getResultsFormatter()->formatResults($feedZoneRequests[$key], $partnerApiResults)) {
                    $feedZoneRequests[$key]->setResultSet($results);
                }

                if($feedZoneRequests[$key]->getInboundParams()->getRequestParams()->isDebug()) {
                    echo $response->getInfo()['url'] . '<br>';
                }

            } catch (\Exception $exception) {

            }
        }
    }

    public function createRawResults(FeedZoneRequest $feedZoneRequest): string
    {
        if (is_null($feedZoneRequest->getPartnerRequestFilters())) {
            return '';
        }

        try {
            $response = $this->getPartnerApi()->createResponse(
                $feedZoneRequest->getPartnerRequestFilters()
            );

            return $response->getContent();
        } catch (\Exception $exception) {

        }
    }

    protected function getRequestFilterFactory(): RequestFilterFactoryInterface
    {
        return $this->requestFilterFactory;
    }

    protected function getPartnerApi(): PartnerApiInterface
    {
        return $this->partnerApi;
    }

    protected function getResultsFormatter(): PartnerResultsFormatterInterface
    {
        return $this->resultsFormatter;
    }
}