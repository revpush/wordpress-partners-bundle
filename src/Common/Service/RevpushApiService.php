<?php

namespace MmgAdsPlugin\Common\Service;

use GuzzleHttp\Client;
use Swagger\Client\Api\AccountApi;
use Swagger\Client\Api\AdgroupApi;
use Swagger\Client\Configuration;
use Swagger\Client\Model\AccessTokenAccountRead;
use Swagger\Client\Model\AccessTokenSettingAccountRead;
use Swagger\Client\Model\AccountAccountRead;
use Swagger\Client\Model\AdgroupAdgroupRead;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class RevpushApiService
{
    private Configuration $apiConfig;
    private CacheInterface $cache;

    const API_URL = 'https://api.revpu.sh';
    const CACHE_TIME = 600;

    public const API_PARTNER_IRI = '/api/partners/';
    public const API_SITE_IRI = '/api/sites/';

    public function __construct(string $authToken, CacheInterface $cache)
    {
        $this->apiConfig = $this->getApiConfig($authToken);
        $this->cache = $cache;
    }

    public static function create(string $authToken, ?CacheInterface $cache = null)
    {
        $cache = is_null($cache) ? new FilesystemAdapter() : $cache;

        return new RevpushApiService(
            $authToken,
            $cache
        );
    }

    /**
     * @param int $yahooAccountId
     * @return  AccessTokenSettingAccountRead[]
     */
    public function getAccessTokenSettings(int $yahooAccountId): array
    {
        $account = $this->getAccount($yahooAccountId);
        $accessTokens = $account->getAccessTokens();

        /** @var AccessTokenAccountRead $accessToken */
        $accessToken = array_shift($accessTokens);
        return $accessToken->getAccessTokenSettings();
    }

    public function getAdgroup(string $linkKey): AdgroupAdgroupRead
    {
        $adgroups = $this->getAdgroups($linkKey);
        return array_shift($adgroups);
    }

    public function getAdgroups(string $linkKey): array
    {
        $adgroupApi = new AdgroupApi(new Client(), $this->apiConfig, null);
        return $this->cache->get('adgroups_' . $linkKey, function (ItemInterface $item) use ($adgroupApi, $linkKey) {
            $item->expiresAfter(self::CACHE_TIME);

            return $adgroupApi->getAdgroupCollection(1, $linkKey);
        });
    }

    public function getAccount(int $yahooAccountId): AccountAccountRead
    {
        $accountApi = new AccountApi(new Client(), $this->apiConfig, null);

        return $this->cache->get('account_' . $yahooAccountId, function (ItemInterface $item) use ($accountApi, $yahooAccountId) {
            $item->expiresAfter(self::CACHE_TIME);

            return $accountApi->getAccountItem($yahooAccountId);
        });
    }

    private function getApiConfig(string $authToken): Configuration
    {
        $apiConfig = new Configuration();
        $apiConfig->setHost(self::API_URL);
        $apiConfig->setApiKey('X-AUTH-TOKEN', $authToken);
        return $apiConfig;
    }
}