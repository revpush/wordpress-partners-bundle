<?php

namespace MmgAdsPlugin\Common\Partner\Common;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface PartnerApiInterface
{
    public function createResponse(PartnerRequestFilters $requestFilters): ResponseInterface;
    public function createResults(ResponseInterface $response): PartnerApiResult;
}