<?php

namespace MmgAdsPlugin\Common\Partner\Common;

class PartnerApiResult
{
    protected array $result = [];

    protected $resultObject;

    public function getResult(): array
    {
        return $this->result;
    }

    public function getResultObject()
    {
        return $this->resultObject;
    }

    public function setResultObject($resultObject): PartnerApiResult
    {
        $this->resultObject = $resultObject;

        return $this;
    }

    public function __get(string $name)
    {
        return $this->result[$name] ?? null;
    }

    public function __set(string $name, $value)
    {
        $this->result[$name] = $value;
    }

    public function __isset(string $name): bool
    {
        return isset($this->result[$name]);
    }

    public function __unset($name)
    {
        unset($this->result[$name]);
    }

    public function getItem(array $elements, array $keys, $default = null)
    {
        if(!$key = array_shift($keys)) {
            return $elements;
        }

        $nextElement = $elements[$key] ?? $default;

        if(!is_array($nextElement)) {
            return $nextElement;
        }

        return $this->getItem($nextElement, $keys, $default);
    }
}