<?php

namespace MmgAdsPlugin\Common\Partner\Common;

abstract class AbstractResultSet
{
    /**
     * @var AbstractAdResult[]
     */
    protected array $ads = [];

    public function addAd(AbstractAdResult $adResult)
    {
        $this->ads[] = $adResult;
    }

    /**
     * @return AbstractAdResult[]
     */
    public function getAds(): array
    {
        return $this->ads;
    }

    /**
     * @param AbstractAdResult[] $ads
     * @return $this
     */
    public function setAds(array $ads): AbstractResultSet
    {
        $this->ads = $ads;

        return $this;
    }

    public function __call(string $name, array $arguments)
    {
        return null;
    }

    public static function __callStatic($name, $arguments)
    {
        return null;
    }
}