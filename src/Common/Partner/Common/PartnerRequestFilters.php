<?php

namespace MmgAdsPlugin\Common\Partner\Common;

abstract class PartnerRequestFilters
{
    private float $timeout = 10;
    private ?string $keyword = null;

    public function getTimeout(): float
    {
        return $this->timeout;
    }

    public function setTimeout(float $timeout): self
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string|null $keyword
     * @return PartnerRequestFilters
     */
    public function setKeyword(?string $keyword): PartnerRequestFilters
    {
        $this->keyword = $keyword;
        return $this;
    }
}