<?php

namespace MmgAdsPlugin\Common\Partner\Common;

interface FactoryApiServiceInterface
{
    public function createService(): PartnerApiInterface;
}