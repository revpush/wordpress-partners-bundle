<?php

namespace MmgAdsPlugin\Common\Partner\Common;

abstract class AbstractAdResult
{
    protected ?string $title = null;
    protected ?string $description = null;
    protected ?string $imageUrl = null;
    protected ?string $displayUrl = null;
    protected ?string $adUrl = null;
    protected ?string $trackUrl = null;
    protected ?float $rpc = null;
    /**
     * @var AbstractAdSitelink[]
     */
    protected array $siteLinks = [];

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return AbstractAdResult
     */
    public function setTitle(?string $title): AbstractAdResult
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return AbstractAdResult
     */
    public function setDescription(?string $description): AbstractAdResult
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     *
     * @return AbstractAdResult
     */
    public function setImageUrl(?string $imageUrl): AbstractAdResult
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDisplayUrl(): ?string
    {
        return $this->displayUrl;
    }

    /**
     * @param string|null $displayUrl
     *
     * @return AbstractAdResult
     */
    public function setDisplayUrl(?string $displayUrl): AbstractAdResult
    {
        $this->displayUrl = $displayUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdUrl(): ?string
    {
        return $this->adUrl;
    }

    /**
     * @param string|null $adUrl
     *
     * @return AbstractAdResult
     */
    public function setAdUrl(?string $adUrl): AbstractAdResult
    {
        $this->adUrl = $adUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrackUrl(): ?string
    {
        return $this->trackUrl;
    }

    /**
     * @param string|null $trackUrl
     *
     * @return AbstractAdResult
     */
    public function setTrackUrl(?string $trackUrl): AbstractAdResult
    {
        $this->trackUrl = $trackUrl;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRpc(): ?float
    {
        return $this->rpc;
    }

    /**
     * @param float|null $rpc
     *
     * @return AbstractAdResult
     */
    public function setRpc(?float $rpc): AbstractAdResult
    {
        $this->rpc = $rpc;

        return $this;
    }

    public function addSiteLink(AbstractAdSitelink $abstractAdSitelink)
    {
        $this->siteLinks[] = $abstractAdSitelink;
    }

    /**
     * @return AbstractAdSitelink[]
     */
    public function getSiteLinks(): array
    {
        return $this->siteLinks;
    }

    /**
     * @param AbstractAdSitelink[] $siteLinks
     *
     * @return AbstractAdResult
     */
    public function setSiteLinks(array $siteLinks): AbstractAdResult
    {
        $this->siteLinks = $siteLinks;

        return $this;
    }

    public function __call(string $name, array $arguments)
    {
        return null;
    }

    public static function __callStatic($name, $arguments)
    {
        return null;
    }
}