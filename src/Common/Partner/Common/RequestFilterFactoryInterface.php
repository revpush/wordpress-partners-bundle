<?php

namespace MmgAdsPlugin\Common\Partner\Common;

use MmgAdsPlugin\Common\InboundParams\InboundParams;

interface RequestFilterFactoryInterface
{
    public function createRequestFilters(InboundParams $inboundParams): PartnerRequestFilters;
}