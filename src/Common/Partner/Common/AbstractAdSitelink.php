<?php

namespace MmgAdsPlugin\Common\Partner\Common;

abstract class AbstractAdSitelink
{
    private ?string $title;
    private ?string $description;
    private ?array $descriptionLines = [];
    private ?string $siteLinkUrl;
    private ?string $siteLinkTrackUrl;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): AbstractAdSitelink
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return AbstractAdSitelink
     */
    public function setDescription(?string $description): AbstractAdSitelink
    {
        $this->description = $description;

        return $this;
    }

    public function addDescriptionLine(string $line): AbstractAdSitelink
    {
        $this->descriptionLines[] = $line;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getDescriptionLines(): ?array
    {
        return $this->descriptionLines;
    }

    /**
     * @param array|null $descriptionLines
     *
     * @return AbstractAdSitelink
     */
    public function setDescriptionLines(?array $descriptionLines): AbstractAdSitelink
    {
        $this->descriptionLines = $descriptionLines;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSiteLinkUrl(): ?string
    {
        return $this->siteLinkUrl;
    }

    /**
     * @param string|null $siteLinkUrl
     *
     * @return AbstractAdSitelink
     */
    public function setSiteLinkUrl(?string $siteLinkUrl): AbstractAdSitelink
    {
        $this->siteLinkUrl = $siteLinkUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSiteLinkTrackUrl(): ?string
    {
        return $this->siteLinkTrackUrl;
    }

    /**
     * @param string|null $siteLinkTrackUrl
     *
     * @return AbstractAdSitelink
     */
    public function setSiteLinkTrackUrl(?string $siteLinkTrackUrl): AbstractAdSitelink
    {
        $this->siteLinkTrackUrl = $siteLinkTrackUrl;

        return $this;
    }
}