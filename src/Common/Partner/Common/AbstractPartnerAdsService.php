<?php

namespace MmgAdsPlugin\Common\Partner\Common;

use MmgAdsPlugin\Common\InboundParams\InboundParams;

abstract class AbstractPartnerAdsService
{
    public function createFeedZoneRequest(InboundParams $inboundParams): FeedZoneRequest
    {
        $feedZoneRequest = new FeedZoneRequest();
        $feedZoneRequest->setInboundParams($inboundParams);

        return $feedZoneRequest;
    }
}