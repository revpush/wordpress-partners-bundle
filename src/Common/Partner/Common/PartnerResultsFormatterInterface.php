<?php

namespace MmgAdsPlugin\Common\Partner\Common;

interface PartnerResultsFormatterInterface
{
    public function formatResults(FeedZoneRequest $feedZoneRequest, PartnerApiResult $partnerApiResult): ?AbstractResultSet;
}