<?php

namespace MmgAdsPlugin\Common\Partner\Common;

use MmgAdsPlugin\Common\InboundParams\InboundParams;

class FeedZoneRequest
{
    private ?InboundParams $inboundParams = null;
    private ?PartnerRequestFilters $partnerRequestFilters = null;
    private AbstractResultSet $resultSet;

    public function __construct()
    {
        $this->resultSet = new NullResultSet();
    }

    /**
     * @return InboundParams|null
     */
    public function getInboundParams(): ?InboundParams
    {
        return $this->inboundParams;
    }

    /**
     * @param InboundParams|null $inboundParams
     * @return FeedZoneRequest
     */
    public function setInboundParams(?InboundParams $inboundParams): FeedZoneRequest
    {
        $this->inboundParams = $inboundParams;
        return $this;
    }

    /**
     * @return PartnerRequestFilters|null
     */
    public function getPartnerRequestFilters(): ?PartnerRequestFilters
    {
        return $this->partnerRequestFilters;
    }

    /**
     * @param PartnerRequestFilters|null $partnerRequestFilters
     * @return FeedZoneRequest
     */
    public function setPartnerRequestFilters(?PartnerRequestFilters $partnerRequestFilters): FeedZoneRequest
    {
        $this->partnerRequestFilters = $partnerRequestFilters;
        return $this;
    }

    /**
     * @return AbstractResultSet
     */
    public function getResultSet(): AbstractResultSet
    {
        return $this->resultSet;
    }

    /**
     * @param AbstractResultSet $resultSet
     * @return FeedZoneRequest
     */
    public function setResultSet(AbstractResultSet $resultSet): FeedZoneRequest
    {
        $this->resultSet = $resultSet;
        return $this;
    }
}