<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo;

use Exception;
use MmgAdsPlugin\Common\Partner\Common\PartnerApiInterface;
use MmgAdsPlugin\Common\Partner\Common\PartnerApiResult;
use MmgAdsPlugin\Common\Partner\Common\PartnerRequestFilters;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class YahooApiService implements PartnerApiInterface
{
    private HttpClientInterface $httpClient;
    private SerializerInterface $serializer;

    public function __construct(
        HttpClientInterface $httpClient,
        SerializerInterface $serializer
    )
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * @param YahooRequestFilters $requestFilters
     *
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function createResponse(PartnerRequestFilters $requestFilters): ResponseInterface
    {
        $query = [
            'Partner' => $requestFilters->getTag(),
            'serveUrl' => $requestFilters->getPageUrl(),
            'mkt' => strtolower($requestFilters->getMarket()),
            'affilData' => $this->getAffilData($requestFilters),
            'Keywords' => $requestFilters->getKeyword(),
            'type' => $requestFilters->getType(),
            'moreSponsoredResults' => 1,
            'keywordCharEnc' => $requestFilters->getCharset(),
            'longAdsTitle' => 1,
            'bolding' => 1,
            'maxCount' => $requestFilters->getAdsCount()
        ];

        if ($requestFilters->isSiteLink()) {
            $query['siteLink'] = (int)$requestFilters->isSiteLink();
        }

        if ($requestFilters->isAdEnable4thLine()) {
            $query['adEnable4thLine'] = (int)$requestFilters->isAdEnable4thLine();
        }

        if ($requestFilters->isAdEnableTopAd()) {
            $query['adEnableTopAd'] = (int)$requestFilters->isAdEnableTopAd();
        }

        if ($requestFilters->isAdSmartAnnotations()) {
            $query['adSmartAnnotations'] = (int)$requestFilters->isAdSmartAnnotations();
        }

        if ($requestFilters->isAdEnableMerchRating()) {
            $query['adEnableMerchRating'] = (int)$requestFilters->isAdEnableMerchRating();
        }

        if ($requestFilters->isOsb()) {
            $query['osb'] = (int)$requestFilters->isOsb();
        }

        if ($requestFilters->isC2c()) {
            $query['C2C'] = (int)$requestFilters->isC2c();
        }

        if ($requestFilters->isEnableFavicon()) {
            $query['enableFavicon'] = (int)$requestFilters->isEnableFavicon();
        }

        if ($requestFilters->isAdEnableCalloutExtension()) {
            $query['adEnableCalloutExtension'] = (int)$requestFilters->isAdEnableCalloutExtension();
        }

        if ($requestFilters->isAdEnableReview()) {
            $query['adEnableReview'] = (int)$requestFilters->isAdEnableReview();
        }

        if ($requestFilters->isAdImageExtensions()) {
            $query['adImageExtensions'] = (int)$requestFilters->isAdImageExtensions();
        }

        if ($requestFilters->isAdEnableActionExt()) {
            $query['adEnableActionExt'] = (int)$requestFilters->isAdEnableActionExt();
        }

        if ($requestFilters->isAdEnhancedSiteLink()) {
            $query['adEnhancedSiteLink'] = (int)$requestFilters->isAdEnhancedSiteLink();
        }

        if ($requestFilters->isAdEnableBrand()) {
            $query['adEnableBrand'] = (int)$requestFilters->isAdEnableBrand();
        }

        if ($requestFilters->isPla()) {
            $query['pla'] = (int)$requestFilters->isPla();
        }
        if ($requestFilters->isAdEnableProductAds()) {
            $query['adEnableProductAds'] = (int)$requestFilters->isAdEnableProductAds();
        }
        if ($requestFilters->isCheckProductAdsEligibility()) {
            $query['checkProductAdsEligibility'] = (int)$requestFilters->isCheckProductAdsEligibility();
        }

        if ($localAds = $requestFilters->getLocalAds()) {
            $query['localAds'] = $localAds;
        }

        if ($requestFilters->isOrganic()) {
            $query['in'] = $requestFilters->getAdsCount();
            $query['on'] = 0;
        } else {
            $query['on'] = $requestFilters->getAdsCount();
            $query['in'] = 0;
        }

        if (count($requestFilters->getUrlFilters()) > 0) {
            $query['urlFilters'] = $requestFilters->getUrlFilters();
        }

        $query['adultFilter'] = !$requestFilters->isAdultFilter() ? 'any' : 'clean';

        $requestParams = [
            'query' => $query,
            'timeout' => $requestFilters->getTimeout()
        ];

        if ($requestFilters->isNeedProxy()) {
            $requestParams['proxy'] = $requestFilters->getProxy();
        }

        return $this->httpClient->request('GET', 'https://' . $requestFilters->getHost() . $requestFilters->getPath(), $requestParams);
    }

    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws Exception|TransportExceptionInterface
     */
    public function createResults(ResponseInterface $response): PartnerApiResult
    {
        if ($response->getStatusCode() != 200) {
            throw new Exception('Response code ' . $response->getStatusCode());
        }

        $content = $response->getContent();

        return $this->serializer->deserialize($content, PartnerApiResult::class, 'xml');
    }

    private function getAffilData(YahooRequestFilters $requestFilters): string
    {
        $affilData = [
            'ip' => $requestFilters->getUserIp(),
            'ua' => $requestFilters->getUserAgent()
        ];

        if ($userXfip = $requestFilters->getUserXfip()) {
            $affilData['xfip'] = $userXfip;
        }

        return http_build_query($affilData);
    }
}
