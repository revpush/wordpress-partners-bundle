<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo;

use MmgAdsPlugin\Common\Partner\Common\PartnerRequestFilters;

class YahooRequestFilters extends PartnerRequestFilters
{
    private string $host;
    private string $path;
    private string $tag;
    private string $type;
    private string $pageUrl;
    private string $market;
    private string $userIp;
    private string $userAgent;
    private ?string $userXfip = null;
    private int $adsCount = 4;
    private bool $adultFilter = true;
    private string $charset = 'utf-8';
    private bool $isOrganic = false;
    private array $urlFilters = [];
    private bool $needProxy = true;
    private ?string $proxy = null;
    private bool $siteLink = true;
    private bool $adEnable4thLine = false;
    private bool $adEnableTopAd = false;
    private bool $adSmartAnnotations = false;
    private bool $adEnableMerchRating = false;
    private bool $osb = false;
    private bool $c2c = false;
    private bool $enableFavicon = false;
    private bool $adEnableCalloutExtension = false;
    private bool $adEnableReview = false;
    private bool $adImageExtensions = false;
    private bool $adEnableActionExt = false;
    private bool $adEnhancedSiteLink = false;
    private bool $adEnableBrand = false;
    private ?string $localAds = 'Mixed';

    private bool $pla = false;
    private bool $adEnableProductAds = false;
    private bool $checkProductAdsEligibility = false;

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     *
     * @return YahooRequestFilters
     */
    public function setHost(string $host): YahooRequestFilters
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return YahooRequestFilters
     */
    public function setPath(string $path): YahooRequestFilters
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     *
     * @return YahooRequestFilters
     */
    public function setTag(string $tag): YahooRequestFilters
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return YahooRequestFilters
     */
    public function setType(string $type): YahooRequestFilters
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     *
     * @return YahooRequestFilters
     */
    public function setPageUrl(string $pageUrl): YahooRequestFilters
    {
        $this->pageUrl = $pageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarket(): string
    {
        return $this->market;
    }

    /**
     * @param string $market
     *
     * @return YahooRequestFilters
     */
    public function setMarket(string $market): YahooRequestFilters
    {
        $this->market = $market;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserIp(): string
    {
        return $this->userIp;
    }

    /**
     * @param string $userIp
     *
     * @return YahooRequestFilters
     */
    public function setUserIp(string $userIp): YahooRequestFilters
    {
        $this->userIp = $userIp;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     *
     * @return YahooRequestFilters
     */
    public function setUserAgent(string $userAgent): YahooRequestFilters
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserXfip(): ?string
    {
        return $this->userXfip;
    }

    /**
     * @param string|null $userXfip
     *
     * @return YahooRequestFilters
     */
    public function setUserXfip(?string $userXfip): YahooRequestFilters
    {
        $this->userXfip = $userXfip;

        return $this;
    }

    /**
     * @return int
     */
    public function getAdsCount(): int
    {
        return $this->adsCount;
    }

    /**
     * @param int $adsCount
     *
     * @return YahooRequestFilters
     */
    public function setAdsCount(int $adsCount): YahooRequestFilters
    {
        $this->adsCount = $adsCount;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdultFilter(): bool
    {
        return $this->adultFilter;
    }

    /**
     * @param bool $adultFilter
     *
     * @return YahooRequestFilters
     */
    public function setAdultFilter(bool $adultFilter): YahooRequestFilters
    {
        $this->adultFilter = $adultFilter;

        return $this;
    }

    /**
     * @return string
     */
    public function getCharset(): string
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     *
     * @return YahooRequestFilters
     */
    public function setCharset(string $charset): YahooRequestFilters
    {
        $this->charset = $charset;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOrganic(): bool
    {
        return $this->isOrganic;
    }

    /**
     * @param bool $isOrganic
     *
     * @return YahooRequestFilters
     */
    public function setIsOrganic(bool $isOrganic): YahooRequestFilters
    {
        $this->isOrganic = $isOrganic;

        return $this;
    }

    /**
     * @return array
     */
    public function getUrlFilters(): array
    {
        return $this->urlFilters;
    }

    /**
     * @param array $urlFilters
     *
     * @return YahooRequestFilters
     */
    public function setUrlFilters(array $urlFilters): YahooRequestFilters
    {
        $this->urlFilters = $urlFilters;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNeedProxy(): bool
    {
        return $this->needProxy;
    }

    /**
     * @param bool $needProxy
     *
     * @return YahooRequestFilters
     */
    public function setNeedProxy(bool $needProxy): YahooRequestFilters
    {
        $this->needProxy = $needProxy;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProxy(): ?string
    {
        return $this->proxy;
    }

    /**
     * @param string|null $proxy
     *
     * @return YahooRequestFilters
     */
    public function setProxy(?string $proxy): YahooRequestFilters
    {
        $this->proxy = $proxy;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSiteLink(): bool
    {
        return $this->siteLink;
    }

    /**
     * @param bool $siteLink
     *
     * @return YahooRequestFilters
     */
    public function setSiteLink(bool $siteLink): YahooRequestFilters
    {
        $this->siteLink = $siteLink;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnable4thLine(): bool
    {
        return $this->adEnable4thLine;
    }

    /**
     * @param bool $adEnable4thLine
     *
     * @return YahooRequestFilters
     */
    public function setAdEnable4thLine(bool $adEnable4thLine): YahooRequestFilters
    {
        $this->adEnable4thLine = $adEnable4thLine;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableTopAd(): bool
    {
        return $this->adEnableTopAd;
    }

    /**
     * @param bool $adEnableTopAd
     *
     * @return YahooRequestFilters
     */
    public function setAdEnableTopAd(bool $adEnableTopAd): YahooRequestFilters
    {
        $this->adEnableTopAd = $adEnableTopAd;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdSmartAnnotations(): bool
    {
        return $this->adSmartAnnotations;
    }

    /**
     * @param bool $adSmartAnnotations
     *
     * @return YahooRequestFilters
     */
    public function setAdSmartAnnotations(bool $adSmartAnnotations): YahooRequestFilters
    {
        $this->adSmartAnnotations = $adSmartAnnotations;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableMerchRating(): bool
    {
        return $this->adEnableMerchRating;
    }

    /**
     * @param bool $adEnableMerchRating
     *
     * @return YahooRequestFilters
     */
    public function setAdEnableMerchRating(bool $adEnableMerchRating): YahooRequestFilters
    {
        $this->adEnableMerchRating = $adEnableMerchRating;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOsb(): bool
    {
        return $this->osb;
    }

    /**
     * @param bool $osb
     *
     * @return YahooRequestFilters
     */
    public function setOsb(bool $osb): YahooRequestFilters
    {
        $this->osb = $osb;

        return $this;
    }

    /**
     * @return bool
     */
    public function isC2c(): bool
    {
        return $this->c2c;
    }

    /**
     * @param bool $c2c
     *
     * @return YahooRequestFilters
     */
    public function setC2c(bool $c2c): YahooRequestFilters
    {
        $this->c2c = $c2c;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableFavicon(): bool
    {
        return $this->enableFavicon;
    }

    /**
     * @param bool $enableFavicon
     *
     * @return YahooRequestFilters
     */
    public function setEnableFavicon(bool $enableFavicon): YahooRequestFilters
    {
        $this->enableFavicon = $enableFavicon;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableCalloutExtension(): bool
    {
        return $this->adEnableCalloutExtension;
    }

    /**
     * @param bool $adEnableCalloutExtension
     *
     * @return YahooRequestFilters
     */
    public function setAdEnableCalloutExtension(bool $adEnableCalloutExtension): YahooRequestFilters
    {
        $this->adEnableCalloutExtension = $adEnableCalloutExtension;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableReview(): bool
    {
        return $this->adEnableReview;
    }

    /**
     * @param bool $adEnableReview
     *
     * @return YahooRequestFilters
     */
    public function setAdEnableReview(bool $adEnableReview): YahooRequestFilters
    {
        $this->adEnableReview = $adEnableReview;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdImageExtensions(): bool
    {
        return $this->adImageExtensions;
    }

    /**
     * @param bool $adImageExtensions
     *
     * @return YahooRequestFilters
     */
    public function setAdImageExtensions(bool $adImageExtensions): YahooRequestFilters
    {
        $this->adImageExtensions = $adImageExtensions;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableActionExt(): bool
    {
        return $this->adEnableActionExt;
    }

    /**
     * @param bool $adEnableActionExt
     *
     * @return YahooRequestFilters
     */
    public function setAdEnableActionExt(bool $adEnableActionExt): YahooRequestFilters
    {
        $this->adEnableActionExt = $adEnableActionExt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnhancedSiteLink(): bool
    {
        return $this->adEnhancedSiteLink;
    }

    /**
     * @param bool $adEnhancedSiteLink
     *
     * @return YahooRequestFilters
     */
    public function setAdEnhancedSiteLink(bool $adEnhancedSiteLink): YahooRequestFilters
    {
        $this->adEnhancedSiteLink = $adEnhancedSiteLink;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableBrand(): bool
    {
        return $this->adEnableBrand;
    }

    /**
     * @param bool $adEnableBrand
     *
     * @return YahooRequestFilters
     */
    public function setAdEnableBrand(bool $adEnableBrand): YahooRequestFilters
    {
        $this->adEnableBrand = $adEnableBrand;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocalAds(): ?string
    {
        return $this->localAds;
    }

    /**
     * @param string|null $localAds
     *
     * @return YahooRequestFilters
     */
    public function setLocalAds(?string $localAds): YahooRequestFilters
    {
        $this->localAds = $localAds;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPla(): bool
    {
        return $this->pla;
    }

    /**
     * @param bool $pla
     * @return YahooRequestFilters
     */
    public function setPla(bool $pla): YahooRequestFilters
    {
        $this->pla = $pla;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdEnableProductAds(): bool
    {
        return $this->adEnableProductAds;
    }

    /**
     * @param bool $adEnableProductAds
     * @return YahooRequestFilters
     */
    public function setAdEnableProductAds(bool $adEnableProductAds): YahooRequestFilters
    {
        $this->adEnableProductAds = $adEnableProductAds;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCheckProductAdsEligibility(): bool
    {
        return $this->checkProductAdsEligibility;
    }

    /**
     * @param bool $checkProductAdsEligibility
     * @return YahooRequestFilters
     */
    public function setCheckProductAdsEligibility(bool $checkProductAdsEligibility): YahooRequestFilters
    {
        $this->checkProductAdsEligibility = $checkProductAdsEligibility;
        return $this;
    }


}

