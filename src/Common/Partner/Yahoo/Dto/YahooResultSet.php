<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

use MmgAdsPlugin\Common\Partner\Common\AbstractResultSet;

class YahooResultSet extends AbstractResultSet
{
    private string $searchId;
    private string $tag;
    private ?string $nextArgs = null;
    private ?string $prevArgs = null;
    private array $suggestions = [];

    public function __construct(string $searchId, string $tag)
    {
        $this->searchId = $searchId;
        $this->tag = $tag;
    }

    public function getSearchId(): string
    {
        return $this->searchId;
    }

    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @return string|null
     */
    public function getNextArgs(): ?string
    {
        return $this->nextArgs;
    }

    /**
     * @param string|null $nextArgs
     *
     * @return YahooResultSet
     */
    public function setNextArgs(?string $nextArgs): YahooResultSet
    {
        $this->nextArgs = $nextArgs;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrevArgs(): ?string
    {
        return $this->prevArgs;
    }

    /**
     * @param string|null $prevArgs
     *
     * @return YahooResultSet
     */
    public function setPrevArgs(?string $prevArgs): YahooResultSet
    {
        $this->prevArgs = $prevArgs;

        return $this;
    }

    /**
     * @return array
     */
    public function getSuggestions(): array
    {
        return $this->suggestions;
    }

    /**
     * @param array $suggestions
     *
     * @return YahooResultSet
     */
    public function setSuggestions(array $suggestions): YahooResultSet
    {
        $this->suggestions = $suggestions;

        return $this;
    }
}