<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooTopAd
{
    private ?string $text = null;
    private bool $isTopAd = true;
    private ?string $adDomainClicks = null;
    private ?string $adDomainName = null;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return YahooTopAd
     */
    public function setText(?string $text): YahooTopAd
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTopAd(): bool
    {
        return $this->isTopAd;
    }

    /**
     * @param bool $isTopAd
     *
     * @return YahooTopAd
     */
    public function setIsTopAd(bool $isTopAd): YahooTopAd
    {
        $this->isTopAd = $isTopAd;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdDomainClicks(): ?string
    {
        return $this->adDomainClicks;
    }

    /**
     * @param string|null $adDomainClicks
     *
     * @return YahooTopAd
     */
    public function setAdDomainClicks(?string $adDomainClicks): YahooTopAd
    {
        $this->adDomainClicks = $adDomainClicks;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdDomainName(): ?string
    {
        return $this->adDomainName;
    }

    /**
     * @param string|null $adDomainName
     *
     * @return YahooTopAd
     */
    public function setAdDomainName(?string $adDomainName): YahooTopAd
    {
        $this->adDomainName = $adDomainName;

        return $this;
    }
}