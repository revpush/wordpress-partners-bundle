<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooReview
{
    private ?string $text = null;
    private ?string $sourceText = null;
    private ?string $url = null;
    private ?string $trackUrl = null;
    private bool $isNonBillable = false;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return YahooReview
     */
    public function setText(?string $text): YahooReview
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSourceText(): ?string
    {
        return $this->sourceText;
    }

    /**
     * @param string|null $sourceText
     *
     * @return YahooReview
     */
    public function setSourceText(?string $sourceText): YahooReview
    {
        $this->sourceText = $sourceText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return YahooReview
     */
    public function setUrl(?string $url): YahooReview
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrackUrl(): ?string
    {
        return $this->trackUrl;
    }

    /**
     * @param string|null $trackUrl
     *
     * @return YahooReview
     */
    public function setTrackUrl(?string $trackUrl): YahooReview
    {
        $this->trackUrl = $trackUrl;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNonBillable(): bool
    {
        return $this->isNonBillable;
    }

    /**
     * @param bool $isNonBillable
     *
     * @return YahooReview
     */
    public function setIsNonBillable(bool $isNonBillable): YahooReview
    {
        $this->isNonBillable = $isNonBillable;

        return $this;
    }
}