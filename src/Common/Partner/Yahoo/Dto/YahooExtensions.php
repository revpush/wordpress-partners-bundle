<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooExtensions
{
    private ?Yahoo4thAnnotation $yahoo4thAnnotation = null;
    private ?YahooAction $yahooAction = null;
    private ?YahooCallout $yahooCallout = null;
    private ?YahooImage $yahooImage = null;
    private ?YahooReview $yahooReview = null;
    /**
     * @var YahooSitelink[]
     */
    private array $yahooSitelinks = [];
    private ?YahooSmartAnnotation $yahooSmartAnnotation = null;
    private ?YahooTopAd $yahooTopAd = null;

    /**
     * @return Yahoo4thAnnotation|null
     */
    public function getYahoo4thAnnotation(): ?Yahoo4thAnnotation
    {
        return $this->yahoo4thAnnotation;
    }

    /**
     * @param Yahoo4thAnnotation|null $yahoo4thAnnotation
     *
     * @return YahooExtensions
     */
    public function setYahoo4thAnnotation(?Yahoo4thAnnotation $yahoo4thAnnotation): YahooExtensions
    {
        $this->yahoo4thAnnotation = $yahoo4thAnnotation;

        return $this;
    }

    /**
     * @return YahooAction|null
     */
    public function getYahooAction(): ?YahooAction
    {
        return $this->yahooAction;
    }

    /**
     * @param YahooAction|null $yahooAction
     *
     * @return YahooExtensions
     */
    public function setYahooAction(?YahooAction $yahooAction): YahooExtensions
    {
        $this->yahooAction = $yahooAction;

        return $this;
    }

    /**
     * @return YahooCallout|null
     */
    public function getYahooCallout(): ?YahooCallout
    {
        return $this->yahooCallout;
    }

    /**
     * @param YahooCallout|null $yahooCallout
     *
     * @return YahooExtensions
     */
    public function setYahooCallout(?YahooCallout $yahooCallout): YahooExtensions
    {
        $this->yahooCallout = $yahooCallout;

        return $this;
    }

    /**
     * @return YahooImage|null
     */
    public function getYahooImage(): ?YahooImage
    {
        return $this->yahooImage;
    }

    /**
     * @param YahooImage|null $yahooImage
     *
     * @return YahooExtensions
     */
    public function setYahooImage(?YahooImage $yahooImage): YahooExtensions
    {
        $this->yahooImage = $yahooImage;

        return $this;
    }

    /**
     * @return YahooReview|null
     */
    public function getYahooReview(): ?YahooReview
    {
        return $this->yahooReview;
    }

    /**
     * @param YahooReview|null $yahooReview
     *
     * @return YahooExtensions
     */
    public function setYahooReview(?YahooReview $yahooReview): YahooExtensions
    {
        $this->yahooReview = $yahooReview;

        return $this;
    }

    public function addYahooSiteLink(YahooSitelink $yahooSitelink)
    {
        $this->yahooSitelinks[] = $yahooSitelink;
    }

    /**
     * @return YahooSitelink[]
     */
    public function getYahooSitelinks(): array
    {
        return $this->yahooSitelinks;
    }

    /**
     * @param YahooSitelink[] $yahooSitelinks
     *
     * @return YahooExtensions
     */
    public function setYahooSitelinks(array $yahooSitelinks): YahooExtensions
    {
        $this->yahooSitelinks = $yahooSitelinks;

        return $this;
    }

    /**
     * @return YahooSmartAnnotation|null
     */
    public function getYahooSmartAnnotation(): ?YahooSmartAnnotation
    {
        return $this->yahooSmartAnnotation;
    }

    /**
     * @param YahooSmartAnnotation|null $yahooSmartAnnotation
     *
     * @return YahooExtensions
     */
    public function setYahooSmartAnnotation(?YahooSmartAnnotation $yahooSmartAnnotation): YahooExtensions
    {
        $this->yahooSmartAnnotation = $yahooSmartAnnotation;

        return $this;
    }

    /**
     * @return YahooTopAd|null
     */
    public function getYahooTopAd(): ?YahooTopAd
    {
        return $this->yahooTopAd;
    }

    /**
     * @param YahooTopAd|null $yahooTopAd
     *
     * @return YahooExtensions
     */
    public function setYahooTopAd(?YahooTopAd $yahooTopAd): YahooExtensions
    {
        $this->yahooTopAd = $yahooTopAd;

        return $this;
    }
}