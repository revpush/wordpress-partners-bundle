<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class Yahoo4thAnnotation
{
    private ?string $text = null;
    private ?string $url = null;
    private ?string $trackUrl = null;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return Yahoo4thAnnotation
     */
    public function setText(?string $text): Yahoo4thAnnotation
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return Yahoo4thAnnotation
     */
    public function setUrl(?string $url): Yahoo4thAnnotation
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrackUrl(): ?string
    {
        return $this->trackUrl;
    }

    /**
     * @param string|null $trackUrl
     *
     * @return Yahoo4thAnnotation
     */
    public function setTrackUrl(?string $trackUrl): Yahoo4thAnnotation
    {
        $this->trackUrl = $trackUrl;

        return $this;
    }
}