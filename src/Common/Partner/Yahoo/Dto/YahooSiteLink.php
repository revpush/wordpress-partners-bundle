<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooSiteLink
{
    private ?string $text = null;
    private ?string $url = null;
    private ?string $trackUrl = null;
    private array $descrLines = [];

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return YahooSiteLink
     */
    public function setText(?string $text): YahooSiteLink
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return YahooSiteLink
     */
    public function setUrl(?string $url): YahooSiteLink
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrackUrl(): ?string
    {
        return $this->trackUrl;
    }

    /**
     * @param string|null $trackUrl
     *
     * @return YahooSiteLink
     */
    public function setTrackUrl(?string $trackUrl): YahooSiteLink
    {
        $this->trackUrl = $trackUrl;

        return $this;
    }

    public function addDescrLine(string $line)
    {
        $this->descrLines[] = $line;
    }

    /**
     * @return array
     */
    public function getDescrLines(): array
    {
        return $this->descrLines;
    }

    /**
     * @param array $descrLines
     *
     * @return YahooSiteLink
     */
    public function setDescrLines(array $descrLines): YahooSiteLink
    {
        $this->descrLines = $descrLines;

        return $this;
    }
}