<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooAction
{
    private ?string $impressionToken = null;
    private ?string $layoutId = null;
    private ?string $displayBlock = null;
    private ?string $actionText = null;
    private ?string $actionUrl = null;
    private ?string $actionTrackUrl = null;
    private ?int $k = null;
    private ?string $ns = null;
    private bool $isNonBillable = false;

    /**
     * @return string|null
     */
    public function getImpressionToken(): ?string
    {
        return $this->impressionToken;
    }

    /**
     * @param string|null $impressionToken
     *
     * @return YahooAction
     */
    public function setImpressionToken(?string $impressionToken): YahooAction
    {
        $this->impressionToken = $impressionToken;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLayoutId(): ?string
    {
        return $this->layoutId;
    }

    /**
     * @param string|null $layoutId
     *
     * @return YahooAction
     */
    public function setLayoutId(?string $layoutId): YahooAction
    {
        $this->layoutId = $layoutId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDisplayBlock(): ?string
    {
        return $this->displayBlock;
    }

    /**
     * @param string|null $displayBlock
     *
     * @return YahooAction
     */
    public function setDisplayBlock(?string $displayBlock): YahooAction
    {
        $this->displayBlock = $displayBlock;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActionText(): ?string
    {
        return $this->actionText;
    }

    /**
     * @param string|null $actionText
     *
     * @return YahooAction
     */
    public function setActionText(?string $actionText): YahooAction
    {
        $this->actionText = $actionText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActionUrl(): ?string
    {
        return $this->actionUrl;
    }

    /**
     * @param string|null $actionUrl
     *
     * @return YahooAction
     */
    public function setActionUrl(?string $actionUrl): YahooAction
    {
        $this->actionUrl = $actionUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActionTrackUrl(): ?string
    {
        return $this->actionTrackUrl;
    }

    /**
     * @param string|null $actionTrackUrl
     *
     * @return YahooAction
     */
    public function setActionTrackUrl(?string $actionTrackUrl): YahooAction
    {
        $this->actionTrackUrl = $actionTrackUrl;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getK(): ?int
    {
        return $this->k;
    }

    /**
     * @param int|null $k
     *
     * @return YahooAction
     */
    public function setK(?int $k): YahooAction
    {
        $this->k = $k;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNs(): ?string
    {
        return $this->ns;
    }

    /**
     * @param string|null $ns
     *
     * @return YahooAction
     */
    public function setNs(?string $ns): YahooAction
    {
        $this->ns = $ns;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNonBillable(): bool
    {
        return $this->isNonBillable;
    }

    /**
     * @param bool $isNonBillable
     *
     * @return YahooAction
     */
    public function setIsNonBillable(bool $isNonBillable): YahooAction
    {
        $this->isNonBillable = $isNonBillable;

        return $this;
    }
}