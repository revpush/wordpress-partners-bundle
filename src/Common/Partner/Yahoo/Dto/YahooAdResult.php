<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

use MmgAdsPlugin\Common\Partner\Common\AbstractAdResult;

class YahooAdResult extends AbstractAdResult
{
    private ?string $impressionId = null;
    private ?int $rank = null;
    private bool $biddedListing = false;
    private ?string $adultRating = null;
    private ?string $resultType = null;
    private ?string $phoneNumber = null;
    private ?string $faviconUrl = null;
    private bool $osb = false;
    private ?string $rating = null;
    private ?string $ratingNumber = null;
    private ?string $ratingPage = null;
    private ?string $ratingStarLink = null;
    private YahooExtensions $extensions;

    public function __construct()
    {
        $this->extensions = new YahooExtensions();
    }

    /**
     * @return string|null
     */
    public function getImpressionId(): ?string
    {
        return $this->impressionId;
    }

    /**
     * @param string|null $impressionId
     *
     * @return YahooAdResult
     */
    public function setImpressionId(?string $impressionId): YahooAdResult
    {
        $this->impressionId = $impressionId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRank(): ?int
    {
        return $this->rank;
    }

    /**
     * @param int|null $rank
     *
     * @return YahooAdResult
     */
    public function setRank(?int $rank): YahooAdResult
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBiddedListing(): bool
    {
        return $this->biddedListing;
    }

    /**
     * @param bool $biddedListing
     *
     * @return YahooAdResult
     */
    public function setBiddedListing(bool $biddedListing): YahooAdResult
    {
        $this->biddedListing = $biddedListing;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdultRating(): ?string
    {
        return $this->adultRating;
    }

    /**
     * @param string|null $adultRating
     *
     * @return YahooAdResult
     */
    public function setAdultRating(?string $adultRating): YahooAdResult
    {
        $this->adultRating = $adultRating;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResultType(): ?string
    {
        return $this->resultType;
    }

    /**
     * @param string|null $resultType
     *
     * @return YahooAdResult
     */
    public function setResultType(?string $resultType): YahooAdResult
    {
        $this->resultType = $resultType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     *
     * @return YahooAdResult
     */
    public function setPhoneNumber(?string $phoneNumber): YahooAdResult
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFaviconUrl(): ?string
    {
        return $this->faviconUrl;
    }

    /**
     * @param string|null $faviconUrl
     *
     * @return YahooAdResult
     */
    public function setFaviconUrl(?string $faviconUrl): YahooAdResult
    {
        $this->faviconUrl = $faviconUrl;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOsb(): bool
    {
        return $this->osb;
    }

    /**
     * @param bool $osb
     *
     * @return YahooAdResult
     */
    public function setOsb(bool $osb): YahooAdResult
    {
        $this->osb = $osb;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRating(): ?string
    {
        return $this->rating;
    }

    /**
     * @param string|null $rating
     *
     * @return YahooAdResult
     */
    public function setRating(?string $rating): YahooAdResult
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRatingNumber(): ?string
    {
        return $this->ratingNumber;
    }

    /**
     * @param string|null $ratingNumber
     *
     * @return YahooAdResult
     */
    public function setRatingNumber(?string $ratingNumber): YahooAdResult
    {
        $this->ratingNumber = $ratingNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRatingPage(): ?string
    {
        return $this->ratingPage;
    }

    /**
     * @param string|null $ratingPage
     *
     * @return YahooAdResult
     */
    public function setRatingPage(?string $ratingPage): YahooAdResult
    {
        $this->ratingPage = $ratingPage;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRatingStarLink(): ?string
    {
        return $this->ratingStarLink;
    }

    /**
     * @param string|null $ratingStarLink
     *
     * @return YahooAdResult
     */
    public function setRatingStarLink(?string $ratingStarLink): YahooAdResult
    {
        $this->ratingStarLink = $ratingStarLink;

        return $this;
    }

    /**
     * @return YahooExtensions
     */
    public function getExtensions(): YahooExtensions
    {
        return $this->extensions;
    }
}