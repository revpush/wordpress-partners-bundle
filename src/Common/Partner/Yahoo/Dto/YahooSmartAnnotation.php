<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooSmartAnnotation
{
    private array $phrases;

    public function __construct(array $phrases = [])
    {
        $this->phrases = $phrases;
    }

    /**
     * @return array
     */
    public function getPhrases(): array
    {
        return $this->phrases;
    }
}