<?php

namespace MmgAdsPlugin\Common\Partner\Yahoo\Dto;

class YahooImage
{
    private ?string $originalMediaHeight = null;
    private ?string $originalMediaWidth = null;
    private ?string $url = null;
    private ?string $trackUrl = null;
    private ?string $mediaUrl = null;
    private ?string $altText = null;

    /**
     * @return string|null
     */
    public function getOriginalMediaHeight(): ?string
    {
        return $this->originalMediaHeight;
    }

    /**
     * @param string|null $originalMediaHeight
     *
     * @return YahooImage
     */
    public function setOriginalMediaHeight(?string $originalMediaHeight): YahooImage
    {
        $this->originalMediaHeight = $originalMediaHeight;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOriginalMediaWidth(): ?string
    {
        return $this->originalMediaWidth;
    }

    /**
     * @param string|null $originalMediaWidth
     *
     * @return YahooImage
     */
    public function setOriginalMediaWidth(?string $originalMediaWidth): YahooImage
    {
        $this->originalMediaWidth = $originalMediaWidth;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return YahooImage
     */
    public function setUrl(?string $url): YahooImage
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrackUrl(): ?string
    {
        return $this->trackUrl;
    }

    /**
     * @param string|null $trackUrl
     *
     * @return YahooImage
     */
    public function setTrackUrl(?string $trackUrl): YahooImage
    {
        $this->trackUrl = $trackUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMediaUrl(): ?string
    {
        return $this->mediaUrl;
    }

    /**
     * @param string|null $mediaUrl
     *
     * @return YahooImage
     */
    public function setMediaUrl(?string $mediaUrl): YahooImage
    {
        $this->mediaUrl = $mediaUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAltText(): ?string
    {
        return $this->altText;
    }

    /**
     * @param string|null $altText
     *
     * @return YahooImage
     */
    public function setAltText(?string $altText): YahooImage
    {
        $this->altText = $altText;

        return $this;
    }
}