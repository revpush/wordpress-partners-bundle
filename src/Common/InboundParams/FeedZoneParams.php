<?php

namespace MmgAdsPlugin\Common\InboundParams;

class FeedZoneParams
{
    private int $count;
    private ?string $keyword = null;
    private string $template;
    private string $theme;

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return FeedZoneParams
     */
    public function setCount(int $count): FeedZoneParams
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string|null $keyword
     * @return FeedZoneParams
     */
    public function setKeyword(?string $keyword): FeedZoneParams
    {
        $this->keyword = $keyword;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return FeedZoneParams
     */
    public function setTemplate(string $template): FeedZoneParams
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string
     */
    public function getTheme(): string
    {
        return $this->theme;
    }

    /**
     * @param string $theme
     * @return FeedZoneParams
     */
    public function setTheme(string $theme): FeedZoneParams
    {
        $this->theme = $theme;
        return $this;
    }
}