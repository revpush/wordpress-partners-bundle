<?php

namespace MmgAdsPlugin\Common\InboundParams;

class InboundParams
{
    private PluginParams $pluginParams;
    private RequestParams $requestParams;
    private FeedZoneParams $feedZoneParams;

    public function __construct(
        PluginParams $pluginParams,
        RequestParams $requestParams,
        FeedZoneParams $feedZoneParams
    )
    {
        $this->pluginParams = $pluginParams;
        $this->requestParams = $requestParams;
        $this->feedZoneParams = $feedZoneParams;
    }

    public function getRequestParams(): RequestParams
    {
        return clone $this->requestParams;
    }

    public function setRequestParams(RequestParams $requestParams): InboundParams
    {
        $copy = clone $this;
        $copy->requestParams = $requestParams;

        return $copy;
    }

    public function getPluginParams(): PluginParams
    {
        return clone $this->pluginParams;
    }

    public function setPluginParams(PluginParams $pluginParams): InboundParams
    {
        $copy = clone $this;
        $copy->pluginParams = $pluginParams;

        return $copy;
    }

    public function getFeedZoneParams(): FeedZoneParams
    {
        return clone $this->feedZoneParams;
    }

    public function setFeedZoneParams(FeedZoneParams $feedZoneParams): InboundParams
    {
        $copy = clone $this;
        $copy->feedZoneParams = $feedZoneParams;

        return $copy;
    }
}