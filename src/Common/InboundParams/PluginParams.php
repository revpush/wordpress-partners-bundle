<?php

namespace MmgAdsPlugin\Common\InboundParams;

class PluginParams
{
    private string $apiKey;
    private string $market;
    private string $domain;
    private string $proxy;
    private string $keywordParamName;
    private int $yahooAccountId;
    private int $siteId;
    private string $defaultYahooTag;

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return PluginParams
     */
    public function setApiKey(string $apiKey): PluginParams
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getMarket(): string
    {
        return $this->market;
    }

    /**
     * @param string $market
     * @return PluginParams
     */
    public function setMarket(string $market): PluginParams
    {
        $this->market = $market;
        return $this;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return PluginParams
     */
    public function setDomain(string $domain): PluginParams
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string
     */
    public function getProxy(): string
    {
        return $this->proxy;
    }

    /**
     * @param string $proxy
     * @return PluginParams
     */
    public function setProxy(string $proxy): PluginParams
    {
        $this->proxy = $proxy;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeywordParamName(): string
    {
        return $this->keywordParamName;
    }

    /**
     * @param string $keywordParamName
     * @return PluginParams
     */
    public function setKeywordParamName(string $keywordParamName): PluginParams
    {
        $this->keywordParamName = $keywordParamName;
        return $this;
    }

    /**
     * @return int
     */
    public function getSiteId(): int
    {
        return $this->siteId;
    }

    /**
     * @param int $siteId
     * @return PluginParams
     */
    public function setSiteId(int $siteId): PluginParams
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * @return int
     */
    public function getYahooAccountId(): int
    {
        return $this->yahooAccountId;
    }

    /**
     * @param int $yahooAccountId
     * @return PluginParams
     */
    public function setYahooAccountId(int $yahooAccountId): PluginParams
    {
        $this->yahooAccountId = $yahooAccountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultYahooTag(): string
    {
        return $this->defaultYahooTag;
    }

    /**
     * @param string $defaultYahooTag
     * @return PluginParams
     */
    public function setDefaultYahooTag(string $defaultYahooTag): PluginParams
    {
        $this->defaultYahooTag = $defaultYahooTag;
        return $this;
    }
}