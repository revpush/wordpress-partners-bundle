<?php

namespace MmgAdsPlugin\Common\InboundParams;

class RequestParams
{
    private ?string $requestUrl = null;
    private ?string $requestPath = null;
    private ?string $userIp = null;
    private ?string $userAgent = null;
    private ?string $userReferer = null;
    private ?string $sessionId = null;
    private ?string $linkKey = null;
    private ?string $keyword = null;
    private ?int $sourceId = null;
    private ?int $siteId = null;
    private ?string $visitorId = null;
    private bool $debug = false;
    private ?int $keywordId = null;

    /**
     * @return string|null
     */
    public function getRequestUrl(): ?string
    {
        return $this->requestUrl;
    }

    /**
     * @param string|null $requestUrl
     * @return RequestParams
     */
    public function setRequestUrl(?string $requestUrl): RequestParams
    {
        $this->requestUrl = $requestUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequestPath(): ?string
    {
        return $this->requestPath;
    }

    /**
     * @param string|null $requestPath
     * @return RequestParams
     */
    public function setRequestPath(?string $requestPath): RequestParams
    {
        $this->requestPath = $requestPath;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserIp(): ?string
    {
        return $this->userIp;
    }

    /**
     * @param string|null $userIp
     * @return RequestParams
     */
    public function setUserIp(?string $userIp): RequestParams
    {
        $this->userIp = $userIp;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @param string|null $userAgent
     * @return RequestParams
     */
    public function setUserAgent(?string $userAgent): RequestParams
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserReferer(): ?string
    {
        return $this->userReferer;
    }

    /**
     * @param string|null $userReferer
     * @return RequestParams
     */
    public function setUserReferer(?string $userReferer): RequestParams
    {
        $this->userReferer = $userReferer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    /**
     * @param string|null $sessionId
     * @return RequestParams
     */
    public function setSessionId(?string $sessionId): RequestParams
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkKey(): ?string
    {
        return $this->linkKey;
    }

    /**
     * @param string|null $linkKey
     * @return RequestParams
     */
    public function setLinkKey(?string $linkKey): RequestParams
    {
        $this->linkKey = $linkKey;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string|null $keyword
     * @return RequestParams
     */
    public function setKeyword(?string $keyword): RequestParams
    {
        $this->keyword = $keyword;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSourceId(): ?int
    {
        return $this->sourceId;
    }

    /**
     * @param int|null $sourceId
     * @return RequestParams
     */
    public function setSourceId(?int $sourceId): RequestParams
    {
        $this->sourceId = $sourceId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSiteId(): ?int
    {
        return $this->siteId;
    }

    /**
     * @param int|null $siteId
     * @return RequestParams
     */
    public function setSiteId(?int $siteId): RequestParams
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVisitorId(): ?string
    {
        return $this->visitorId;
    }

    /**
     * @param string|null $visitorId
     * @return RequestParams
     */
    public function setVisitorId(?string $visitorId): RequestParams
    {
        $this->visitorId = $visitorId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return RequestParams
     */
    public function setDebug(bool $debug): RequestParams
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getKeywordId(): ?int
    {
        return $this->keywordId;
    }

    /**
     * @param int|null $keywordId
     * @return RequestParams
     */
    public function setKeywordId(?int $keywordId): RequestParams
    {
        $this->keywordId = $keywordId;
        return $this;
    }
}