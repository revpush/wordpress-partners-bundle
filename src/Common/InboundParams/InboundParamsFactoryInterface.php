<?php

namespace MmgAdsPlugin\Common\InboundParams;

interface InboundParamsFactoryInterface
{
    public function create(array $atts = []): InboundParams;
}