<?php

namespace MmgAdsPlugin\Common\InboundParams;

interface FeedZoneParamsFactoryInterface
{
    public function create(): FeedZoneParams;
}