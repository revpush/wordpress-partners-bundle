<?php

namespace MmgAdsPlugin\Common\InboundParams;

interface RequestParamsFactoryInterface
{
    public function create(): RequestParams;
}