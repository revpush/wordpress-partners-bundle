<?php

namespace MmgAdsPlugin\Common\InboundParams;

interface PluginParamsFactoryInterface
{
    public function create(): PluginParams;
}