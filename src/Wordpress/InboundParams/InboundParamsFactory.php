<?php

namespace MmgAdsPlugin\Wordpress\InboundParams;

use MmgAdsPlugin\Common\InboundParams\InboundParams;
use MmgAdsPlugin\Common\InboundParams\InboundParamsFactoryInterface;

class InboundParamsFactory implements InboundParamsFactoryInterface
{
    public function create(array $atts = []): InboundParams
    {
        $pluginParamsFactory = new PluginParamsFactory();
        $pluginParams = $pluginParamsFactory->create();

        $requestParamsFactory = new RequestParamsFactory($pluginParams);
        $requestParams = $requestParamsFactory->create();

        $feedZoneParamsFactory = new FeedZoneParamsFactory($atts);
        $feedZoneParams = $feedZoneParamsFactory->create();

        return new InboundParams($pluginParams, $requestParams, $feedZoneParams);
    }
}