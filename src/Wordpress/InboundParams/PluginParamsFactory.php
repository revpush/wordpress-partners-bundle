<?php

namespace MmgAdsPlugin\Wordpress\InboundParams;


use MmgAdsPlugin\Common\InboundParams\PluginParams;
use MmgAdsPlugin\Common\InboundParams\PluginParamsFactoryInterface;

class PluginParamsFactory implements PluginParamsFactoryInterface
{
    const MARKET = ['gid' => 'mmg_ads_market', 'name' => 'Market'];
    const DOMAIN = ['gid' => 'mmg_ads_domain', 'name' => 'Domain'];
    const PROXY = ['gid' => 'mmg_ads_proxy', 'name' => 'Proxy'];
    const SETTING_API_KEY = ['gid' => 'mmg_ads_api_key', 'name' => 'Api Key'];
    const YAHOO_ACCOUNT_ID = ['gid' => 'mmg_ads_yahoo_account_id', 'name' => 'Yahoo Account Id'];
    const KEYWORD_PARAM_NAME = ['gid' => 'mmg_ads_keyword_param_name', 'name' => 'YKeyword Param Name'];
    const SITE_ID = ['gid' => 'mmg_ads_site_id', 'name' => 'Site Id'];
    const DEFAULT_YAHOO_TAG = ['gid' => 'mmg_ads_default_yahoo_tag', 'name' => 'Default Yahoo Tag'];

    public function create(): PluginParams
    {
        $this->validateOptions();

        $params = new PluginParams();
        $params
            ->setApiKey(get_option(self::SETTING_API_KEY['gid']))
            ->setMarket(get_option(self::MARKET['gid']))
            ->setDomain(get_option(self::DOMAIN['gid']))
            ->setProxy(get_option(self::PROXY['gid']))
            ->setKeywordParamName(get_option(self::KEYWORD_PARAM_NAME['gid']))
            ->setYahooAccountId((int) get_option(self::YAHOO_ACCOUNT_ID['gid']))
            ->setSiteId((int) get_option(self::SITE_ID['gid']))
            ->setDefaultYahooTag((string) get_option(self::DEFAULT_YAHOO_TAG['gid']))
        ;

        return $params;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function validateOptions(): void
    {
        if (!get_option(self::MARKET['gid'])) {
            throw new \Exception('Market is not exists');
        }

        if (!get_option(self::PROXY['gid'])) {
            throw new \Exception('Proxy is not exists');
        }

        if (!get_option(self::DOMAIN['gid'])) {
            throw new \Exception('Domain is not exists');
        }

        if (!get_option(self::KEYWORD_PARAM_NAME['gid'])) {
            throw new \Exception('Keyword param is not exists');
        }

        if (!get_option(self::SETTING_API_KEY['gid'])) {
            throw new \Exception('Api Key is not exists');
        }

        if (!get_option(self::YAHOO_ACCOUNT_ID['gid'])) {
            throw new \Exception('Yahoo account id is not exists');
        }

        if (!get_option(self::SITE_ID['gid'])) {
            throw new \Exception('Site id is not exists');
        }

        if (!get_option(self::DEFAULT_YAHOO_TAG['gid'])) {
            throw new \Exception('Default Yahoo tag is not exists');
        }
    }
}