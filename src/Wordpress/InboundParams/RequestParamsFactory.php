<?php

namespace MmgAdsPlugin\Wordpress\InboundParams;

use MmgAdsPlugin\Common\InboundParams\PluginParams;
use MmgAdsPlugin\Common\InboundParams\RequestParams;
use MmgAdsPlugin\Common\InboundParams\RequestParamsFactoryInterface;

class RequestParamsFactory implements RequestParamsFactoryInterface
{
    private PluginParams $pluginParams;

    private const LINK_KEY_PARAM = 'link_key';
    private const PARTNER_SOURCE_ID_PARAM = 'p_id';
    private const SITE_ID_PARAM = 's_id';
    private const VISITOR_ID_PARAM = 'visitor_id';
    private const API_DISPLAY_REQUEST = 'api_display_request';
    private const KEYWORD_ID = 'k_id';

    public function __construct(PluginParams $pluginParams)
    {
        $this->pluginParams = $pluginParams;
    }

    public function create(): RequestParams
    {
        $requestUrl = $this->getRequestUrl();

        $requestParams = new RequestParams();
        $requestParams
            ->setRequestUrl($requestUrl)
            ->setRequestPath($this->getRequestPath($requestUrl))
            ->setUserAgent($this->getUserAgent())
            ->setUserIp($this->getUserIp())
            ->setUserReferer($this->getUserReferer())
            ->setSessionId($this->getSessionId())
        ;

        $queryUrl = parse_url($requestParams->getRequestUrl(), PHP_URL_QUERY);
        parse_str($queryUrl, $query);

        $requestParams
            ->setLinkKey($this->getParamValueFromQuery($query, self::LINK_KEY_PARAM))
            ->setKeyword($this->getParamValueFromQuery($query, $this->pluginParams->getKeywordParamName()))
            ->setSourceId($this->getParamValueFromQuery($query, self::PARTNER_SOURCE_ID_PARAM))
            ->setSiteId($this->getParamValueFromQuery($query, self::SITE_ID_PARAM))
            ->setVisitorId($this->getParamValueFromQuery($query, self::VISITOR_ID_PARAM))
            ->setDebug((bool) $this->getParamValueFromQuery($query, self::API_DISPLAY_REQUEST, false))
            ->setKeywordId($this->getParamValueFromQuery($query, self::KEYWORD_ID))
        ;

        return $requestParams;
    }

    private function getUserAgent(): ?string
    {
        return $_SERVER['HTTP_USER_AGENT'] ?? null;
    }

    private function getUserIp(): ?string
    {
        return $_SERVER['REMOTE_ADDR'] ?? null;
    }

    private function getUserReferer(): ?string
    {
        return $_SERVER['HTTP_REFERER'] ?? null;
    }

    private function getSessionId(): ?string
    {
        return session_id();
    }

    private function getRequestPath(string $requestUrl): string
    {
        $url = explode('?', $requestUrl);
        return $url[0];
    }

    private function getRequestUrl(): string
    {
        return ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    private function getParamValueFromQuery(array $query, string $key, $default = null)
    {
        return $query[$key] ?? $default;
    }
}