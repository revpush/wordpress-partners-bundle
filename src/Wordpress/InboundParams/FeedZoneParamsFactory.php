<?php

namespace MmgAdsPlugin\Wordpress\InboundParams;

use MmgAdsPlugin\Common\InboundParams\FeedZoneParams;
use MmgAdsPlugin\Common\InboundParams\FeedZoneParamsFactoryInterface;

class FeedZoneParamsFactory implements FeedZoneParamsFactoryInterface
{
    private array $atts;

    public const COUNT = 'count';
    public const KEYWORD = 'keyword';
    public const TEMPLATE = 'template';
    public const THEME = 'theme';

    public function __construct(array $atts = [])
    {
        $this->atts = $atts;
    }

    public function create(): FeedZoneParams
    {
        $feedZoneParams = new FeedZoneParams();
        $feedZoneParams
            ->setKeyword($this->getAttsValue(self::KEYWORD))
            ->setCount($this->getAttsValue(self::COUNT, 4))
            ->setTemplate($this->getAttsValue(self::TEMPLATE, 'mmg-y-template'))
            ->setTheme($this->getAttsValue(self::THEME, 'mmg-orange-theme'))
        ;

        return $feedZoneParams;

    }

    private function getAttsValue(string $key, $default = null)
    {
        return $this->atts[$key] ?? $default;
    }
}