<?php

namespace MmgAdsPlugin\Wordpress\Partner\Yahoo;

use MmgAdsPlugin\Common\InboundParams\InboundParams;
use MmgAdsPlugin\Common\Partner\Common\PartnerRequestFilters;
use MmgAdsPlugin\Common\Partner\Common\RequestFilterFactoryInterface;
use MmgAdsPlugin\Common\Partner\Yahoo\YahooRequestFilters;
use MmgAdsPlugin\Common\Service\RevpushApiService;
use Swagger\Client\Model\AccessTokenSettingAccountRead;

class YahooRequestFilterFactory implements RequestFilterFactoryInterface
{
    private const ACCESS_TOKEN_SETTINGS_TAG = 'tag';

    public function createRequestFilters(InboundParams $inboundParams): PartnerRequestFilters
    {
        if(!$keyword = $inboundParams->getFeedZoneParams()->getKeyword() ?? $inboundParams->getRequestParams()->getKeyword()) {
            throw new \Exception('Keyword is not set');
        }

        $yahooRequestFilters = new YahooRequestFilters();
        $yahooRequestFilters
            ->setKeyword($keyword)
            ->setAdsCount($inboundParams->getFeedZoneParams()->getCount())
            ->setHost($this->getHost($inboundParams->getPluginParams()->getMarket()))
            ->setPath('/d/search/p/moneymedia/xmlb/multi/')
            ->setMarket($inboundParams->getPluginParams()->getMarket())
            ->setPageUrl($inboundParams->getRequestParams()->getRequestPath())
            ->setUserIp($inboundParams->getRequestParams()->getUserIp())
            ->setUserAgent($inboundParams->getRequestParams()->getUserAgent())
            ->setTag($this->getCurrentTag($inboundParams) ?? $inboundParams->getPluginParams()->getDefaultYahooTag())
            ->setType($this->generateType($inboundParams))
            ->setNeedProxy(true)
            ->setProxy($inboundParams->getPluginParams()->getProxy())
            ->setAdEnable4thLine(true)
            ->setAdEnableTopAd(true)
            ->setAdSmartAnnotations(true)
            ->setAdEnableCalloutExtension(true)
            ->setAdEnhancedSiteLink(true)
            ->setAdEnableActionExt(true)
            ->setAdImageExtensions(true)
        ;

        return $yahooRequestFilters;
    }

    private function getHost(string $market): string
    {
        return ($market === 'us') ? 'xml-nar-ss.ysm.yahoo.com' : 'xml-eu-ss.ysm.yahoo.com';
    }

    private function getCurrentTag(InboundParams $inboundParams): ?string
    {
        $tag = null;

        try {
            $revpushApiService = RevpushApiService::create($inboundParams->getPluginParams()->getApiKey());
            $accessTokenSettings = $revpushApiService->getAccessTokenSettings($inboundParams->getPluginParams()->getYahooAccountId());

            $accessTokenSettingsTags = array_filter($accessTokenSettings, function ($accessTokenSetting) {
                return $accessTokenSetting->getSettingKey() === self::ACCESS_TOKEN_SETTINGS_TAG;
            });

            $tag = $this->getTagFromAccessTokenSettings($accessTokenSettingsTags, $inboundParams);
        } catch (\Exception $exception) {}

        return $tag;
    }

    /**
     * @param AccessTokenSettingAccountRead[] $accessTokenSettings
     * @return string|null
     */
    private function getTagFromAccessTokenSettings(array $accessTokenSettings, InboundParams $inboundParams): ?string
    {
        $siteUri = RevpushApiService::API_SITE_IRI . $inboundParams->getPluginParams()->getSiteId();
        $partnerUri = RevpushApiService::API_PARTNER_IRI . $inboundParams->getRequestParams()->getSourceId();

        foreach ($accessTokenSettings as $accessTokenSetting) {
            if (
                $accessTokenSetting->getSite() === $siteUri &&
                $accessTokenSetting->getPartner() === $partnerUri
            ) {
                return $accessTokenSetting->getSettingValue();
            }
        }

        foreach ($accessTokenSettings as $accessTokenSetting) {
            if (
                $accessTokenSetting->getPartner() === null &&
                $accessTokenSetting->getSite() === $siteUri
            ) {
                return $accessTokenSetting->getSettingValue();
            }
        }

        return null;
    }

    private function generateType(InboundParams $inboundParams): string
    {
        $domain = $inboundParams->getPluginParams()->getDomain();
        $linkKey = $inboundParams->getRequestParams()->getLinkKey();
        $keywordId = $inboundParams->getRequestParams()->getKeywordId();

        return $linkKey ? $linkKey . '.' . $keywordId . '.' . $domain : $domain;
    }
}