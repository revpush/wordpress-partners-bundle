<?php

namespace MmgAdsPlugin\Wordpress\Partner\Yahoo;

use MmgAdsPlugin\Common\Partner\Common\AbstractResultSet;
use MmgAdsPlugin\Common\Partner\Common\FeedZoneRequest;
use MmgAdsPlugin\Common\Partner\Common\PartnerApiResult;
use MmgAdsPlugin\Common\Partner\Common\PartnerResultsFormatterInterface;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooAction;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooAdResult;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooCallout;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooImage;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooResultSet;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooSiteLink;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooSmartAnnotation;
use MmgAdsPlugin\Common\Partner\Yahoo\Dto\YahooTopAd;

class YahooResultFormatter implements PartnerResultsFormatterInterface
{
    public function formatResults(FeedZoneRequest $feedZoneRequest, PartnerApiResult $partnerApiResult): ?AbstractResultSet
    {
        $yahooRequestFilters = $feedZoneRequest->getPartnerRequestFilters();
        $result = $partnerApiResult->getResult();

        if(!$searchId = $partnerApiResult->getItem($result, ['SearchID'])) {
            throw new \Exception('Empty SearchId param');
        }

        $resultSet = new YahooResultSet($searchId, $yahooRequestFilters->getTag());
        $resultSet
            ->setNextArgs($partnerApiResult->getItem($result, ['ResultSet', 'NextArgs']))
            ->setPrevArgs($partnerApiResult->getItem($result, ['ResultSet', 'PrevArgs']))
        ;

        if($suggestions = $partnerApiResult->getItem($result, ['MoreSponsoredResult', 'Suggestion', 'name'], [])) {
            if(is_string($suggestions)) {
                $suggestions = [$suggestions];
            }

            $resultSet->setSuggestions($suggestions);
        }

        if($ads = $partnerApiResult->getItem($result, ['ResultSet', 'Listing'])) {
            if($title = $partnerApiResult->getItem($ads, ['@title'])) {
                $adObject = $this->parseAdObject($ads, $partnerApiResult, 0);
                $resultSet->addAd($adObject);
            } else {
                foreach($ads as $index => $ad) {
                    $adObject = $this->parseAdObject($ad, $partnerApiResult, $index);
                    $resultSet->addAd($adObject);
                }
            }
        }

        return $resultSet;
    }

    private function parseAdObject(
        $ad,
        PartnerApiResult $partnerApiResult,
        int $adPosition
    ): YahooAdResult
    {
        $adObject = $this->createAdObject($ad, $partnerApiResult, $adPosition);

        $extensions = $adObject->getExtensions();
        if($siteLinks = $partnerApiResult->getItem($ad, ['Extensions', 'SiteLinks', 'SiteLink'])) {
            foreach($siteLinks as $index => $siteLink) {
                $siteLinkUrl = $partnerApiResult->getItem($siteLink, ['Url', 'Url']);

                $siteLinkObject = new YahooSiteLink();
                $siteLinkObject
                    ->setText($partnerApiResult->getItem($siteLink, ['Text']))
                    ->setUrl($siteLinkUrl)
                    ->setTrackUrl($siteLinkUrl)
                ;

                if($description1 = $partnerApiResult->getItem($siteLink, ['Description1'])) {
                    $siteLinkObject->addDescrLine($description1);
                }

                if($description2 = $partnerApiResult->getItem($siteLink, ['Description2'])) {
                    $siteLinkObject->addDescrLine($description2);
                }

                $extensions->addYahooSiteLink($siteLinkObject);
            }
        }

        if($smartAnnotation = $partnerApiResult->getItem($ad, ['Extensions', 'SmartAnnotation'])) {
            $phrases = $partnerApiResult->getItem($smartAnnotation, ['Phrases']);
            $smartAnnotationObject = new YahooSmartAnnotation($phrases);
            $extensions->setYahooSmartAnnotation($smartAnnotationObject);
        }

        if($calloutExtension = $partnerApiResult->getItem($ad, ['Extensions', 'calloutExtension'])) {
            $phrases = $partnerApiResult->getItem($calloutExtension, ['phrases']);
            $calloutExtensionObject = new YahooCallout($phrases);
            $extensions->setYahooCallout($calloutExtensionObject);
        }

        if($topAd = $partnerApiResult->getItem($ad, ['Extensions', 'topAdExtension'])) {
            $topAdObject = new YahooTopAd();
            $topAdObject
                ->setText($partnerApiResult->getItem($topAd, ['text']))
                ->setIsTopAd((bool) $partnerApiResult->getItem($topAd, ['isTopAd']))
                ->setAdDomainClicks($partnerApiResult->getItem($topAd, ['adDomainClicks']))
                ->setAdDomainName($partnerApiResult->getItem($topAd, ['adDomainName']))
            ;

            $extensions->setYahooTopAd($topAdObject);
        }

        if($actionExtension = $partnerApiResult->getItem($ad, ['Extensions', 'actionExtension'])) {
            $actionUrl = $partnerApiResult->getItem($actionExtension, ['actionItem', 'link']);
            $actionExtensionObject = new YahooAction();
            $actionExtensionObject
                ->setImpressionToken($partnerApiResult->getItem($actionExtension, ['impressionToken']))
                ->setLayoutId($partnerApiResult->getItem($actionExtension, ['layoutId']))
                ->setDisplayBlock($partnerApiResult->getItem($actionExtension, ['displayBlock']))
                ->setActionUrl($actionUrl)
                ->setIsNonBillable((bool)$partnerApiResult->getItem($actionExtension, ['actionItem', 'isNonBillable']))
                ->setActionText($partnerApiResult->getItem($actionExtension, ['actionItem', 'text']))
            ;

            $extensions->setYahooAction($actionExtensionObject);
        }

        if($imageExtension = $partnerApiResult->getItem($ad, ['Extensions', 'imageExtension', 'image'])) {
            $url = $partnerApiResult->getItem($imageExtension, ['destinationUrlurl']);

            $imageExtensionObject = new YahooImage();
            $imageExtensionObject
                ->setUrl($url)
                ->setMediaUrl($partnerApiResult->getItem($imageExtension, ['mediaUrl']))
                ->setOriginalMediaHeight($partnerApiResult->getItem($imageExtension, ['originalMediaHeight']))
                ->setOriginalMediaWidth($partnerApiResult->getItem($imageExtension, ['originalMediaWidth']))
                ->setAltText($partnerApiResult->getItem($imageExtension, ['altText']))
            ;

            $extensions->setYahooImage($imageExtensionObject);
        }

        return $adObject;
    }

    private function createAdObject(
        $ad,
        PartnerApiResult $partnerApiResult,
        int $adPosition
    ): YahooAdResult
    {
        $adUrl = $partnerApiResult->getItem($ad, ['ClickUrl', '#']);
        $displayUrl = $partnerApiResult->getItem($ad, ['@siteHost']);

        $adObject = new YahooAdResult();
        $adObject
            ->setTitle($partnerApiResult->getItem($ad, ['@title']))
            ->setDescription($partnerApiResult->getItem($ad, ['@description']))
            ->setDisplayUrl($displayUrl)
            ->setRank($partnerApiResult->getItem($ad, ['@rank']))
            ->setBiddedListing($partnerApiResult->getItem($ad, ['@biddedListing']))
            ->setAdultRating($partnerApiResult->getItem($ad, ['@adultRating']))
            ->setResultType($partnerApiResult->getItem($ad, ['@resultType']))
            ->setImpressionId((string) $partnerApiResult->getItem($ad, ['@ImpressionId']))
            ->setPhoneNumber($partnerApiResult->getItem($ad, ['@phoneNumber']))
            ->setFaviconUrl($partnerApiResult->getItem($ad, ['@faviconUrl']))
            ->setOsb($partnerApiResult->getItem($ad, ['@OSB'], false))
            ->setRating($partnerApiResult->getItem($ad, ['@MRRating'], false))
            ->setRatingNumber($partnerApiResult->getItem($ad, ['@MRNumberOfRatings'], false))
            ->setRatingPage($partnerApiResult->getItem($ad, ['@MRDetailPageLink'], false))
            ->setRatingStarLink($partnerApiResult->getItem($ad, ['@MRStarLink'], false))
            ->setAdUrl($adUrl)
            ->setTrackUrl($displayUrl)
        ;

        return $adObject;
    }
}