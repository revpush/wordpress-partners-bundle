<?php

namespace MmgAdsPlugin\Wordpress\Partner\Yahoo;

use MmgAdsPlugin\Common\InboundParams\InboundParams;
use MmgAdsPlugin\Common\Service\PartnerAdsService;

class YahooAdsService extends PartnerAdsService
{
    public static function create(): PartnerAdsService
    {
        $requestFilterFactory = (new \MmgAdsPlugin\Wordpress\Partner\Yahoo\YahooRequestFilterFactory());
        $partnerApi = (new \MmgAdsPlugin\Wordpress\Partner\Yahoo\YahooApiServiceFactory())->createService();
        $resultsFormatter = new \MmgAdsPlugin\Wordpress\Partner\Yahoo\YahooResultFormatter();

        return new YahooAdsService(
            $requestFilterFactory,
            $partnerApi,
            $resultsFormatter
        );
    }
}