<?php

namespace MmgAdsPlugin\Wordpress\Partner\Yahoo;

use MmgAdsPlugin\Common\Partner\Common\FactoryApiServiceInterface;
use MmgAdsPlugin\Common\Partner\Common\PartnerApiInterface;
use MmgAdsPlugin\Common\Partner\Yahoo\YahooApiService;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class YahooApiServiceFactory implements FactoryApiServiceInterface
{
    public function createService(): PartnerApiInterface
    {
        $encoders = ['xml' => new XmlEncoder(), 'json' => new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        return new YahooApiService(
            new CurlHttpClient(),
            new Serializer($normalizers, $encoders)
        );
    }
}