<?php

namespace MmgAdsPlugin\Wordpress\Pages;

class MenuComposite extends MenuComponent
{
    protected array $children = [];

    public function add(MenuComponent $component): void
    {
        $component->setParent($this);
        $this->children[] = $component;
    }

    public function initialize(): void
    {
        parent::initialize();
        /** @var MenuComponent $child */
        foreach($this->children as $child) {
            $child->initialize();
        }
    }

    public function isComposite(): bool
    {
        return true;
    }
}