<?php

namespace MmgAdsPlugin\Wordpress\Pages;

abstract class MenuComponent
{
    public const PAGE_TITLE = 'pageTitle';
    public const MENU_TITLE = 'menuTitle';
    public const CAPABILITY = 'capability';
    public const MENU_SLUG = 'menuSlug';
    public const OPTION_GROUP = 'optionGroup';

    protected array $menuData;
    protected array $fieldsData;
    protected ?MenuComponent $parent = null;

    public function initialize()
    {
        add_action('admin_menu', array($this, 'initializeMenu'), 25);
        add_action('admin_init', array($this, 'initializeMenuFields'));
    }

    public function initializeMenu(): void
    {
        if($parent = $this->getParent()) {
            add_submenu_page(
                $parent->getMenuOption(self::MENU_SLUG),
                $this->getMenuOption(self::PAGE_TITLE),
                $this->getMenuOption(self::MENU_TITLE),
                $this->getMenuOption(self::CAPABILITY),
                $this->getMenuOption(self::MENU_SLUG),
                array($this, 'displayMenu')
            );
        } else {
            add_menu_page(
                $this->getMenuOption(self::PAGE_TITLE),
                $this->getMenuOption(self::MENU_TITLE),
                $this->getMenuOption(self::CAPABILITY),
                $this->getMenuOption(self::MENU_SLUG),
                array($this, 'displayMenu')
            );
        }
    }

    public function displayMenu()
    {
        echo '<div class="wrap">
			<h1>' . get_admin_page_title() . '</h1>
			<form method="post" action="options.php">';

        settings_fields($this->getMenuOption(self::OPTION_GROUP));
        do_settings_sections($this->getMenuOption(self::MENU_SLUG));
        submit_button();

        echo '</form></div>';
    }

    public function initializeMenuFields()
    {
        foreach($this->fieldsData as $field) {
            register_setting($this->getMenuOption(self::OPTION_GROUP), $field['gid'], 'sanitize_text_field');
        }

        add_settings_section('slider_settings_section_id', '', '', $this->getMenuOption(self::MENU_SLUG));

        foreach($this->fieldsData as $field) {
            add_settings_field(
                $field['gid'],
                $field['name'],
                array($this, 'initializeMenuField'),
                $this->getMenuOption(self::MENU_SLUG),
                'slider_settings_section_id',
                array(
                    'label_for' => $field['gid'],
                    'name' => $field['gid'],
                )
            );
        }
    }

    public function initializeMenuField(array $args = [])
    {
        $value = get_option($args['name']);

        printf(
            '<input type="text" id="%s" name="%s" value="%s" />',
            esc_attr($args['name']),
            esc_attr($args['name']),
            sanitize_text_field($value)
        );
    }

    public function getParent(): ?MenuComponent
    {
        return $this->parent;
    }

    public function setParent(?MenuComponent $parent): MenuComponent
    {
        $this->parent = $parent;
        return $this;
    }

    protected function getMenuOption(string $key)
    {
        return $this->menuData[$key] ?? null;
    }

    public function isComposite(): bool
    {
        return false;
    }
}