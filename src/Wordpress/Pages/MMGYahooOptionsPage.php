<?php

namespace MmgAdsPlugin\Wordpress\Pages;

use MmgAdsPlugin\Wordpress\InboundParams\PluginParamsFactory;

class MMGYahooOptionsPage extends MenuComponent
{
    protected array $menuData = [
        self::PAGE_TITLE => 'MMG Ads Plugin',
        self::MENU_TITLE => 'Yahoo Options',
        self::CAPABILITY => 'administrator',
        self::MENU_SLUG => 'mmg_ads_options_yahoo',
        self::OPTION_GROUP => 'mmg_ads_options_yahoo_group'
    ];

    protected array $fieldsData = [
        PluginParamsFactory::YAHOO_ACCOUNT_ID,
        PluginParamsFactory::DEFAULT_YAHOO_TAG
    ];
}