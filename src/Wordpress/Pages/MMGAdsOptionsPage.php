<?php

namespace MmgAdsPlugin\Wordpress\Pages;

use MmgAdsPlugin\Wordpress\InboundParams\PluginParamsFactory;

class MMGAdsOptionsPage extends MenuComposite
{
    protected array $menuData = [
        self::PAGE_TITLE => 'Main Options',
        self::MENU_TITLE => 'MMG Ads Plugin',
        self::CAPABILITY => 'administrator',
        self::MENU_SLUG => 'mmg_ads_options',
        self::OPTION_GROUP => 'mmg_ads_options_group'
    ];

    protected array $fieldsData = [
        PluginParamsFactory::SETTING_API_KEY,
        PluginParamsFactory::MARKET,
        PluginParamsFactory::KEYWORD_PARAM_NAME,
        PluginParamsFactory::PROXY,
        PluginParamsFactory::DOMAIN,
        PluginParamsFactory::SITE_ID
    ];
}