<?php

/*
 * Plugin Name: MMG Ads Plugin
 * Version: 1.0.2
 * Requires PHP: 7.4
 * License: GPL v2 or later
 */

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

$pluginOptions = new \MmgAdsPlugin\Wordpress\Pages\MMGAdsOptionsPage();
$pluginOptions->add((new \MmgAdsPlugin\Wordpress\Pages\MMGYahooOptionsPage()));
$pluginOptions->initialize();

function getMMGAds($atts, $content = null)
{
    $atts = shortcode_atts( array(
        'count' => 3,
        'keyword' => null,
        'theme' => 'mmg-orange-theme',
        'template' => 'mmg-y-list-template'
    ), $atts);

    try {
        $inboundParams = (new \MmgAdsPlugin\Wordpress\InboundParams\InboundParamsFactory())->create($atts);
        $yahooAdsService = \MmgAdsPlugin\Wordpress\Partner\Yahoo\YahooAdsService::create();

        $feedZoneRequests = [];
        $yahooTopFeedZoneRequest = $yahooAdsService->createFeedZoneRequest($inboundParams);
        $feedZoneRequests[] = $yahooTopFeedZoneRequest;

        if($content) {
            $yahooBottomFeedZoneRequest = $yahooAdsService->createFeedZoneRequest($inboundParams);
            $feedZoneRequests[] = $yahooBottomFeedZoneRequest;
        }

        $yahooAdsService->createResults($feedZoneRequests);

        ob_start();
        getMMGContent($yahooTopFeedZoneRequest);
        if($content) {
            echo $content;
            getMMGContent($yahooBottomFeedZoneRequest);
        }
        return ob_get_clean();

    } catch (\Throwable $exception) {

    }
}

function getMMGContent(\MmgAdsPlugin\Common\Partner\Common\FeedZoneRequest $feedZoneRequest)
{
    $template = $feedZoneRequest->getInboundParams()->getFeedZoneParams()->getTemplate();
    if (file_exists(__DIR__ . "/templates/{$template}.php")) {
        require "templates/{$template}.php";
    }
}

function addCssMmgOrangeTemplate() {
    wp_register_style( 'mmg-orange-template', plugins_url('mmg-ads-plugin/assets/mmg-orange-template.css'));
    wp_enqueue_style( 'mmg-orange-template');
}

add_action( 'wp_enqueue_scripts','addCssMmgOrangeTemplate');


add_shortcode('mmg_ads', 'getMMGAds');
